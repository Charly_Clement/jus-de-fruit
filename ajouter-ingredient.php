<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Ajouter Ingrédients</title>
</head>

<body>

    <?php include 'nav-admin.php' ?>

    
    <div class="ajout-ingredient-form">
        <h2>Ajouter un ingrédient</h2>
        <form method="POST" enctype="multipart/form-data" ><br>
            <div class="ajout-ingredient-div-gauche">
                <label for="nom">Nom</label><br><br>
                <input type="text" name="nom_ajouter_ingredient" id="nomnom-ajouter-ingredient" maxlength="30"><br><br>

                <label for="description"></label>Description<br><br>
                <textarea name="description" id="description" cols="30" rows="10"></textarea><br><br>

                <input type="submit" name="submit" value="Sauvegarder" id="submit">
            </div>

            <div class="ajout-ingredient-div-droite">
                <label for="photo-ingredient">Photo de l'Ingrédient</label><br><br>
                <input type="file" name="photo_ingredient" id="photo-ingredient"><br><br>

                <label for="type">Type</label><br><br>
                <select name="type_select" id="type-select">
                    <option value=""></option>
                    <option value="fruit">Fruit</option>
                    <option value="legume">Légume</option>
                    <option value="epice">&Eacute;pice</option>
                </select>
            </div>
        </form>
    </div>

    <?php

        $nom = isset($_POST['nom_ajouter_ingredient']) && !empty($_POST['nom_ajouter_ingredient']) ? $_POST['nom_ajouter_ingredient'] : ''; 
        $description = isset($_POST['description']) && !empty($_POST['description']) ? $_POST['description'] : '';
        $recup = isset($_POST['type_select'])? $_POST['type_select'] : '';
        $submit = isset($_POST['submit']) && !empty($_POST['submit']) ? $_POST['submit'] : ''; 

        if($recup == 'fruit'){
                        $type = 2;
                    } elseif($recup == 'legume'){
                        $type = 1;
                    } else {
                        $type = 3;
                    }

        if($submit){      
            
            $filename = 'upload/' . microtime() . $_FILES['photo_ingredient']['name'];
            move_uploaded_file($_FILES['photo_ingredient']['tmp_name'],$filename);
            $chemin = 'http://localhost/Jus_de_fruit/'.$filename;

            try{

                $sql=$bdd->prepare("INSERT INTO Ingredients (nom,description,photo,id_type) VALUES (:nom,:description,:photo,:type)");
                $sql ->execute(['nom'=>$nom,'description'=>$description,'photo'=>$chemin,'type'=>$type]);
                header('Location: ingredients-et-recettes.php');

            } catch (PDOException $e){
                echo 'Erreur '.$e;
            }

        }

    ?>




</body>
</html>