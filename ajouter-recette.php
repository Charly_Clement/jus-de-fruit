<!DOCTYPE html>

<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ajouter recette</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>

    <?php include 'nav-visiteur.php'; ?>

    <div class="ajout-ingredient-form">
        <h2>Ajouter une recette</h2>
        <form method="POST" enctype="multipart/form-data"><br>
            <div class="ajout-ingredient-div-gauche">
                <label for="nom">Nom de votre recette</label><br><br>
                <input type="text" name="nom_recette" id="nom" maxlength="100"><br><br>

                <label for="description"></label>Description<br><br>
                <textarea name="description" id="description" cols="30" rows="10"></textarea><br><br>

                <input type="submit" name='submit' value="Choisir les ingrédient" id="ajouter-ingredient-submit">
            </div>

            <div class="ajout-ingredient-div-droite">
                <label for="photo-ingredient">Photo de votre recette</label><br><br>
                <input type="file" name="photo_recette" id="photo-ingredient"><br><br>
            </div>
        </form>
    </div>

    <?php

        $nom = isset($_POST['nom_recette']) && !empty($_POST['nom_recette']) ? $_POST['nom_recette'] : '';
        $description = isset($_POST['description']) && !empty($_POST['description']) ? $_POST['description'] : '';
        $filename = isset($_FILES['photo_recette']) ? 'upload/' . microtime() . $_FILES['photo_recette']['name'] : '';
        $submit = isset($_POST['submit']) && !empty($_POST['submit']) ? $_POST['submit'] : '';



        if ($submit) {

            move_uploaded_file($_FILES['photo_recette']['tmp_name'], $filename);
            $chemin = 'http://localhost/Jus_de_fruit/' . $filename;

            try {

                $sql = $bdd->prepare("INSERT INTO recette (nom,description,photo) VALUES (:nom,:description,:photo)");
                $sql->execute(['nom' => $nom, 'description' => $description, 'photo' => $chemin]);
                header('Location: editer-ingredient.php');
            } catch (PDOException $e) {
                echo 'Erreur ' . $e->getMessage();
            }
        }
        ?>

</body>

</html>