<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Connexion</title>
</head>

<body>

    <?php include 'nav-admin.php' ?>

    <div class="connexion-form">
        <h3>Connexion</h3>
        <form method="POST"><br>
            <label for="identifiant" id="connexion-label">Identifiant</label><br><br>
            <input type="text" name="identifiant" id="identifiant" maxlength="30"><br><br><br>
            <label for="mdp" id="connexion-label">Mot de passe</label><br><br>
            <input type="password" name="mdp" id="mdp"><br><br>

            <input type="submit" name="submit" value="Connexion" id="connexion-submit">
        </form>
    </div>

    <?php

        $identifiant = isset($_POST['identifiant']) && !empty($_POST['identifiant']) ? $_POST['identifiant'] : '';
        $mdp         = isset($_POST['mdp'])         && !empty($_POST['mdp'])         ? $_POST['mdp']         : '';
        $submit      = isset($_POST['submit'])      && !empty($_POST['submit'])      ? $_POST['submit']      : ''; 

        if($submit){
            try{
                $sql=$bdd->prepare("SELECT * FROM administrateur WHERE identifiant=:id");
                $sql ->execute(['id'=>$identifiant]);
                $result = $sql->fetchAll();
                foreach($result as $admin){
                    $mp = $admin['mot_de_passe'];
                    $id = $admin['identifiant'];
                }
                if($identifiant == $id){
                    if($mdp == $mp){
                        header('Location: ingredients-et-recettes.php');
                 } else {
                        echo 'Mot de passe invalide';
                    }
                }else {
                    echo 'Identifiant invalide';
                }
            } catch (PDOException $e){
                echo 'Erreur '.$e;
            }

        }
    ?>

</body>
</html>