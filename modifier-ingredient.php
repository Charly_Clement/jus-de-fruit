<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Modifier Ingrédient</title>
</head>

<body>

    <?php include 'nav-admin.php' ?>

    <?php

        $id_ingredient = $_GET['id'];

        try {

            $requete = $bdd->prepare("SELECT * FROM Ingredients WHERE id_ingredient=?");
            $requete -> execute([$id_ingredient]);
            $requete = $requete -> fetchAll();
            $ingredient = $requete[0];
        }
        catch(PDOException $e) {
            echo 'erreur: ' . $e->getMessage();
        }

    ?>

    <?php

        $nom = isset($_POST['nom_modifier_ingredient']) && !empty($_POST['nom_modifier_ingredient']) ? $_POST['nom_modifier_ingredient'] : ''; 
        $description = isset($_POST['description']) && !empty($_POST['description']) ? $_POST['description'] : '';
        $recup = isset($_POST['type_select'])? $_POST['type_select'] : '';
        $filename = isset($_FILES['photo_modifier_ingredient']) ? 'upload/' . microtime() . $_FILES['photo_modifier_ingredient']['name'] : '';
        $submit = isset($_POST['submit']) && !empty($_POST['submit']) ? $_POST['submit'] : ''; 

        if($recup == 'fruit'){
                        $type = 2;
                    } elseif($recup == 'legume'){
                        $type = 1;
                    } else {
                        $type = 3;
                    }

        if($submit){

            move_uploaded_file($_FILES['photo_modifier_ingredient']['tmp_name'],$filename);
            $chemin = 'http://localhost/Jus_de_fruit/'.$filename;
            
            try{

                $sql=$bdd->prepare("UPDATE Ingredients SET nom=:nom,description=:description,photo=:photo,id_type=:type WHERE id_ingredient=:id");
                $sql ->execute(['nom'=>$nom,'description'=>$description,'photo'=>$chemin,'type'=>$type,'id'=>$id_ingredient]);
                header('Location: ingredients-et-recettes.php');

            } catch (PDOException $e){
                echo 'Erreur '.$e->getMessage();
            }
        }

    ?>

    <div class="ajout-ingredient-form">
        <h2>Modifier: <?php echo $ingredient['nom']; ?></h2>
        <form method="POST"><br>
            <div class="modifier-ingredient-div-gauche">
                <label for="nom">Nom</label><br><br>
                <input type="text" name="nom_modifier_ingredient" value="<?php echo $ingredient['nom']; ?>" id="nom_modifier_ingredient"><br><br>

                <label for="description"></label>Description<br><br>
                <textarea name="description" id="description_modifier-ingredient" cols="30" rows="10"><?php echo $ingredient['description']; ?></textarea><br><br>

                <input type="submit" name="submit" value="Sauvegarder" id="submit">
            </div>

            <div class="modifier-ingredient-div-droite">
                <label for="photo-modifier-ingredient">Photo de l'Ingrédient</label><br><br>
                <input type="file" name="photo_modifier_ingredient" id="photo-ingredient"><br><br>

                <label for="type">Type</label><br><br>
                <select name="type_select" id="type-select">
                    <option value=""></option>
                    <option value="legume"<?php if($ingredient['id_type'] == 1) {echo 'selected';} ?> >Légume</option>
                    <option value="fruit" <?php if($ingredient['id_type'] == 2) {echo 'selected';} ?> >Fruit</option>
                    <option value="epice" <?php if($ingredient['id_type'] == 3) {echo 'selected';} ?> >&Eacute;pice</option>
                </select>
            </div>
        </form>
    </div>


</body>
</html>