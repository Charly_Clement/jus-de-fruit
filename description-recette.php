<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Description Recette</title>
</head>

<body>

    <?php include 'nav-visiteur.php' ?>

    <?php

    $id_recette = $_GET['id'];

    try {
        $requete = $bdd->prepare("SELECT * FROM recette WHERE id_recette=?");
        $requete->execute([$id_recette]);
        $requete = $requete->fetchAll();
        $recette = $requete[0];
    } catch (PDOException $e) {
        echo 'erreur: ' . $e->getMessage();
    }

    ?>

    <div id="container">
        <div id="ingredient">
            <div>
                <h2><?php echo $recette['nom']; ?></h2>
            </div>
            <div>
                <img src="<?php echo $recette['photo']; ?>">
            </div>
        </div>
        <div id="descriptif">
            <div>
                <p><?php echo $recette['description']; ?></p>
            </div>
            <div>
                <h3>
                    Ingrédients :
                </h3>
                <?php

                    try {

                        $sql = $bdd->prepare("SELECT ingredient_recette.nb_portion,Ingredients.nom
                            FROM ingredient_recette
                            RIGHT JOIN recette
                            ON recette.id_recette = ingredient_recette.id_recette 
                            RIGHT JOIN Ingredients
                            ON Ingredients.id_ingredient = ingredient_recette.id_ingredient
                            WHERE ingredient_recette.id_recette=?");
                        $sql->execute([$id_recette]);
                        $result = $sql->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur: ' . $e->getMessage();
                    }

                    echo '<p>';
                    foreach ($result as $ingredient) {
                        echo '- ' . $ingredient['nb_portion'] . ' portion' . ($ingredient['nb_portion'] > 1 ? "s" : "") . ' de ' . $ingredient['nom'] . ($ingredient['nb_portion'] > 1 ? "s" : "") . '<br>';
                    }

                ?>

                </p>
            </div>
        </div>
    </div>



</body>

</html>