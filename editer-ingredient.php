<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>&Eacute;diter Ingrédient</title>
</head>

<body>

    <?php include 'nav-visiteur.php' ?>

    <div class="marge-gauche-modifier-recette">
        <h3>Jus de fraises</h3>

        <b>Les ingrédients</b>

        <p>- 5 portions de fraise <input type="checkbox" name="portion-1"></p>
        <p>- 2 portions de orange <input type="checkbox" name="portion-2"></p>

        <label for="quantite"><b>Quantité</b></label><br><br>
        <input type="text" name="quantite" id="quantite">

        <p>portions de</p>

        <select name="type_select" id="type-select">
            <option value=""></option>
            <option value="ananas">Ananas</option>
            <option value="tomate">Tomate</option>
        </select>

        <input type="submit" name="ajouter-ingredient-dans-modification-de-recette" value="Ajouter" id="submit"><br><br><br>

        <input type="submit" name="valider-recette" value="Valider la recette" id="submit">
    </div>

</body>
</html>