CREATE TABLE administrateur(
        id_admin     INT            NOT NULL       AUTO_INCREMENT,
        identifiant  VARCHAR(50)    NOT NULL,
        mdp          VARCHAR(50)    NOT NULL,
        PRIMARY KEY (id_admin)
);

CREATE TABLE type_ingredient(
        id_type     INT             NOT NULL        AUTO_INCREMENT,
        libelle     VARCHAR(50)     NOT NULL,
        PRIMARY KEY (id_type)
);

CREATE TABLE ingredient(
        id_ingredient       INT             NOT NULL        AUTO_INCREMENT,
        nom                 VARCHAR(50)     NOT NULL,
        description         TEXT            NOT NULL,
        photo               VARCHAR(100)    NOT NULL,
        id_type             INT             NOT NULL,
        PRIMARY KEY (id_ingredient),
        FOREIGN KEY (id_type) REFERENCES type_ingredient(id_type)
);

CREATE TABLE recette(
        id_recette          INT             NOT NULL        AUTO_INCREMENT,
        nom                 VARCHAR(100)    NOT NULL,
        description         TEXT            NOT NULL,
        photo               VARCHAR(100)    NOT NULL,
        PRIMARY KEY (id_recette)
);

CREATE TABLE ingredient_recette(
        id_recette          INT             NOT NULL,
        id_ingredient       INT             NOT NULL,
        nb_portion          INT             NOT NULL,
        FOREIGN KEY (id_recette) REFERENCES recette(id_recette),
        FOREIGN KEY (id_ingredient) REFERENCES ingredient(id_ingredient),
        PRIMARY KEY (id_recette,id_ingredient)
);