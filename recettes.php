<!DOCTYPE html>
<html lang="fr">
    
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Recettes</title>
</head>

<body>

    <?php include 'nav-visiteur.php' ?>

    <h2>Vous êtes plutôt : <a id="turquoise" href="page-3-jus-fruits.php"> Jus de fruits </a> /
        <a id="turquoise" href="page-4-jus-legumes.php"> Jus de légumes</a> ?
    </h2>

    <div id="container-ingredient-recette">

        <?php
            // RECUPERE NOM ET PHOTO DES RECETTES
            try {

                $requete = $bdd->prepare("SELECT * FROM recette");
                $requete->execute();
                $requete = $requete->fetchAll();
            }
            catch (PDOException $e) {
                echo 'erreur: ' . $e->getMessage();
            }

            // AFFICHE NOM ET PHOTO DES RECETTES
            foreach ($requete as $recette) {

                echo '
                    <div class="carte-recette">
                        <div class="image">
                            <img  class="img-full" src="'.$recette['photo'].'">
                        </div>
                        <div class="texte">
                            <a class="lien-accueil" href="description-recette.php?id='.$recette['id_recette'].'">'.$recette['nom'].'</a>
                        </div>
                    </div>';
            
            }
        ?>

        <?php

        $ingredient = isset($_POST['ingredient']) && !empty($_POST['ingredient']) ? $_POST['ingredient'] :'';
        $submit     = isset($_POST['submit']);

        // 
        if ($submit) {
            try {
                $requete = $bdd->prepare("SELECT recette.nom
                    FROM ingredient_recette
                    RIGHT JOIN recette
                    ON recette.id_recette = ingredient_recette.id_recette 
                    RIGHT JOIN Ingredients
                    ON Ingredients.id_ingredient = ingredient_recette.id_ingredient
                    WHERE Ingredients.nom=?");
                $requete->execute([$ingredient]);
                $requete = $requete->fetchAll();
                var_dump($requete);
                /* header('Location: recettes.php'); */

            }
            catch(PDOException $e) {
                echo 'Erreur: '.$e->getMessage();
            }
        }

        foreach ($requete as $recette) {

            echo '
                <div class="carte-recette">
                    <div class="image">
                        <img  class="img-full" src="'.$recette['photo'].'">
                    </div>
                    <div class="texte">
                        <a class="lien-accueil" href="recettes.php?id='.$recette['id_recette'].'">'.$recette['nom'].'</a>
                    </div>
                </div>';

        }

        ?>

    </div>

    <div id="partage">
        <h2 id="gris">
            Vous êtes prêts à partager vos idées créatives ?
        </h2>
        <div>
            <a id="turquoise" href="ajouter-recette.php">Soumettez vos recettes de jus</a>
        </div>
    </div>




</body>

</html>