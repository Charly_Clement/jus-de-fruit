<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Ingrédients et Recettes</title>
</head>

<body>

    <?php include 'nav-admin.php' ?>

    <div class="container-complet-ingredient-recette">
        <div class="container-ingredient-recette">
            <div class="ingredient-ajouter-div-gauche">
                <h3 class="titre-ingredient">Ingrédients</h3>
                <a class="bouton-retour-ajouter-ingredient" href="ajouter-ingredient.php">Ajouter</a><br><br>
            </div><br>

            <?php

            // RECUPERE LE NOM DES INGREDIENTS
            try {

                $requete = $bdd->prepare("SELECT id_ingredient,nom FROM Ingredients");
                $requete->execute();
                $requete = $requete->fetchAll();
            } catch (PDOException $e) {
                echo 'erreur: ' . $e->getMessage();
            }

            foreach ($requete as $ingredient) {
                echo '<div class="detail-ingredient">
                            <span>
                                <a class="lien-ingredient-vers-modifier-ingredient" href="modifier-ingredient.php?id=' . $ingredient['id_ingredient'] . '">' . $ingredient['nom'] . '</a>
                            </span><br>
                        </div>';
            }

            ?>

        </div>

        <div class="container-recette-ingredient">
            <div>
                <h3 class="titre-recette">Recettes</h3>
            </div><br>

            <?php

            // RECUPERE LE NOM DES RECETTES
                try {

                    $requete = $bdd->prepare("SELECT id_recette,nom FROM recette");
                    $requete->execute();
                    $requete = $requete->fetchAll();
                }
                catch (PDOException $e) {
                    echo 'erreur: ' . $e->getMessage();
                }

                foreach ($requete as $recette) {
                    echo '<div class="detail-recette">
                            <span><a class="lien-ingredient-vers-modifier-ingredient" href="modifier-recette.php?id=' . $recette['id_recette'] . '">' . $recette['nom'] . '</a></span><br>
                        </div>';
                }

            ?>

        </div>
    </div>

</body>
</html>