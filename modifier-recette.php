<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Modifier recette</title>
</head>

<body>

    <?php include 'nav-admin.php' ?>

    <?php

        $id_recette = $_GET['id'];

        try {

            $requete = $bdd->prepare("SELECT * FROM recette WHERE id_recette=?");
            $requete -> execute([$id_recette]);
            $requete = $requete -> fetchAll();
            $recette = $requete[0];
        }
        catch(PDOException $e) {
            echo 'erreur: ' . $e->getMessage();
        }

    ?>

    <div class="modifier-recette-form">
        <h2>Modifier: <?php echo $recette['nom']; ?> </h2>
        <form method="POST"><br>
            <div class="modifier-recette-div-gauche">
                <label for="nom">Nom de votre recette</label><br><br>
                <input type="text" name="nom_modifier_recette" value="<?php echo $recette['nom']; ?>" id="nom_modifier_recette"><br><br>

                <label for="description"></label>Description de votre recette<br><br>
                <textarea name="description" id="description_modifier-recette" cols="30" rows="10"><?php echo $recette['description']; ?></textarea><br><br>

                <input type="submit" value="&Eacute;diter les ingrédients" name="submit" id="submit">
            </div>

            <div class="modifier-recette-div-droite">
                <label for="photo-modifier-recette">Photo de votre recette</label><br><br>
                <input type="file" name="photo_modifier_recette" id="photo-recette">
            </div>
        </form>
    </div>

    <?php

    $nom = isset($_POST['nom_modifier_recette']) && !empty($_POST['nom_modifier_recette']) ? $_POST['nom_modifier_recette'] : '';
    $description = isset($_POST['description'])  && !empty($_POST['description'])     ? $_POST['description']         : '';
    $filename = isset($_FILES['photo_modifier_recette']) ? 'upload/' . microtime() . $_FILES['photo_modifier_recette']['name'] : '';
    $submit      = isset($_POST['submit'])      && !empty($_POST['submit'])      ? $_POST['submit']      : ''; 

    if($submit) {
        try {

            $requete = $bdd->prepare("UPDATE recette SET nom=:nom,description=:description,photo=:photo WHERE id_recette=:id");
            $requete -> execute(['nom'=>$nom,'description'=>$description,'photo'=>$filename,'id'=>$id_recette]);
            var_dump($requete);
        }
        catch(PDOException $e) {
            echo 'erreur: ' . $e->getMessage();
        }
    }
    ?>


</body>
</html>